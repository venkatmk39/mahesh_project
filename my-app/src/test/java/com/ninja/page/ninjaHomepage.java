package com.ninja.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import com.commonfunctions.Actions;
import com.externalfiles.csvfiles;

public class ninjaHomepage {
	public WebDriver driver;
	public ninjaHomepage(WebDriver driver) {
		this.driver = driver;
	}
//	csvfiles csv = new csvfiles();
//	Actions act = new Actions();
	
	@FindBy(xpath="//input[@name='search']")
	WebElement element;
	
	@FindBy(xpath="//span[contains(text(),\"Account\")]")
	WebElement MyAccount;
	
	@FindBy(xpath = "//a[contains(text(),\"Register\")]")
	public WebElement Register;
	
	@FindBy(xpath = "//a[contains(text(),\"Login\")]")
	public WebElement Login;
	
	@FindBy(xpath="//h1[contains(text(),\"Account\")]")
	public WebElement RegHeader;
	@FindBy(xpath="//h2[contains(text(),\"Account\")]")
	public WebElement MyAccountheader;
	
//	@FindAll({
//		@FindBy(xpath= "//*[@id="top-links"]/ul/li[2]/ul/li[2]/a"),
//		@FindBy(className ="firstname")
//	})
//	public List<WebElement> firstname;
//	
//	@FindAll({
//		@FindBy(id="input-lastname"),
//		@FindBy(className ="lastname")
//	})
//	public List<WebElement> lastname;
	
	@FindBy(id="input-firstname")
	public List<WebElement> firstname;
	
	@FindBy(id="input-lastname")
	public List<WebElement> lastname;
	
	@FindBy(name="email")
	public WebElement email;
	
	@FindBy(name="telephone")
	public WebElement telephone;
	
	@FindBy(name="password")
	public WebElement password;
	
	@FindBy(name="confirm")
	public WebElement confirmpassword;
	
	@FindBy(xpath="//input[@type=\"checkbox\"]")
	public WebElement Checkbox;
	
	@FindBy(xpath="//input[@type=\"submit\"]")
	public WebElement Continuebutton;
	
	@FindBy(id="input-email")
	public WebElement Email;
	
	@FindBy(id="input-password")
	public WebElement loginpass;
	
	@FindBy(xpath="//input[@type=\"submit\"]")
	public WebElement loginbutton;
	
	public void Register() {
		com.commonfunctions.Actions.click(MyAccount);
		com.commonfunctions.Actions.mouseActions(Register);	
		com.commonfunctions.Actions.pagedisplay(RegHeader, "Acc");
	}
	public void Login() {
		com.commonfunctions.Actions.click(MyAccount);
		com.commonfunctions.Actions.mouseActions(Login);	
	}
	public void logincustomer() {
		try {
			
	
		String abcd = csvfiles.local("abc");
		String[] a = abcd.split(",");
		String pass = csvfiles.passwor("password");
		String[] p = pass.split(",");
		
		for (int i = 0; i < a.length + p.length; i++) {
			com.commonfunctions.Actions.Entertext(Email,a[i].toString());
			com.commonfunctions.Actions.Entertext(loginpass,p[i].toString());
			System.err.println(p[i].toString());
			com.commonfunctions.Actions.click(loginbutton);
			String myaccounth = com.commonfunctions.Actions.getText(MyAccountheader);
			System.out.println(myaccounth);
			if(myaccounth!=null && myaccounth!="") {
				if (myaccounth.equals("my account")) {
						System.out.println(Actions.getText(MyAccountheader));
				} else {
					System.out.println("User login is failed");
						break;
				}
	
			}
			}
		} catch (Exception e) {
			System.out.println("error"+e);
		}
		
	}
	public void NewRegister(String text,String last,String tel,String emai,String pas,String pass) {
		firstname(text);
		lastname(last);
		addressdetails(tel,emai);
		password(pas);
		confirmpassword(pass);
		com.commonfunctions.Actions.click(Checkbox);
		com.commonfunctions.Actions.click(Continuebutton);
	}
	private void password(String pas) {
		com.commonfunctions.Actions.Entertext(password, pas);
	}
	private void confirmpassword(String text) {
		com.commonfunctions.Actions.Entertext(confirmpassword, text);
		
	}
	private void addressdetails(String tel,String emai) {
		com.commonfunctions.Actions.Entertext(telephone, tel);
		com.commonfunctions.Actions.Entertext(email, emai);
		
	}
	private void lastname(String text) {
		lastname.forEach(name->{

			com.commonfunctions.Actions.Entertext(name, text);
		});
	}
	private void firstname(String text) {
		firstname.forEach(first->{
			com.commonfunctions.Actions.Entertext(first,text);
		});
		
	}
}
