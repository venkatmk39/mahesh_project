package com.ninja.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import com.commonfunctions.Actions;

public class searchitems extends ninjaHomepage{
	public WebDriver driver;
	
	public searchitems(WebDriver driver) {
		super(driver);
		this.driver = driver;	
	}
	@FindBy(xpath="//input[@name=\"search\"]")
	public WebElement Searchfield;
	
	@FindBy(xpath="//div[@id=\"search\"]//button")
	public WebElement Searchbutton;
	
	@FindBy(xpath="//button[contains(@onclick,\"cart\")]")
	public WebElement Addtocart;
	
	@FindBy(xpath="//p[@class=\"text-right\"]/a[1]")
	public WebElement viewcart;
	
	@FindAll({
		@FindBy(id="cart")
	})
	public List<WebElement> cartitems;
	
	@FindBy(xpath="//h1[contains(.,\"Use Gift Certificate\")]")
	public WebElement Gift;
	
	public void Searchdata(String text) {
		Actions.Entertext(Searchfield, text);
		Actions.click(Searchbutton);
	}
	
	public void Addcart() {
		try {
		Actions.click(Addtocart);
		cart();
		System.err.println("Viewcart");
		Actions.mouseActions(viewcart);
		Actions.getText(Gift);
		}
		catch (Exception e) {
			
		}
	}

	private void cart() {
		cartitems.forEach(name->{
			Actions.javacripteecutor(name);
			System.out.println("Enter cart details");
		});
		
	}

}
