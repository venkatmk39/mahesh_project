package com.utilites;

public class Configurationprop {
	
	public static String propertiesfile = System.getProperty("user.dir")+"\\config\\";
	public static String Constants_excell = System.getProperty("user.dir"+"\\Dummy.xlsx");
	
	public static final String CONSTANTS_CHROME_PROPERTY = "webdriver.chrome.driver";
	public static final String CONSTANTS_IE_PROPERTY = "webdriver.ie.driver";
	public static final String CONSTANTS_PHANTOM_PROPERTY = "phantomjs.binary.path";
	
	// Driver Path Details
		public static String CONSTANTS_CHROME_DRIVER_PATH="drivers\\chrome\\chromedriver1.exe";
		public static String CONSTANTS_IE_DRIVER_PATH;
		public static String CONSTANTS_PHANTOM_DRIVER_PATH;
		public static String CONSTANTS_GECKO_DRIVER_PATH;

}
