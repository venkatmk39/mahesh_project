package com.commonfunctions;



import static org.testng.Assert.assertTrue;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.externalfiles.Loadprop;


public class Actions extends Loadprop {
//	public static WebDriver driver;
	public static  org.openqa.selenium.interactions.Actions mouse;
//	public Actions(WebDriver driver){
//		Actions.driver = driver;
//	}
	/*
	 * 
	 * 
	 */
	public static WebElement click(WebElement element) {
		try {
			webdriverwaitvisible(30, element);
			element.click();	
		} catch (Exception e) {
			
		}
		return element;
	}
	/*
	 * 
	 * 
	 */
	public static WebElement webdriverwaitvisible(int time, WebElement element) {
		try {
			new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			
		}
		return element;
	}
	/*
	 * 
	 * 
	 */
	public static WebElement mouseActions(WebElement element) {
		try {
			webdriverwaitvisible(50, element);
			mouse = new org.openqa.selenium.interactions.Actions(driver);
			mouse.moveToElement(element).click().build().perform();
			
		} catch (Exception e) {
			
		}
		return element;	
	}
	/*
	 * 
	 */
	public static WebElement Entertext(WebElement element,String text) {
		try {
			webdriverwaitvisible(40, element);
			element.clear();
			element.sendKeys(text);		
		} catch (Exception e) {
			
		}
		return element;
	}
	/*
	 * 
	 * 
	 */
	public static void pagedisplay(WebElement element,String text) {
		text = text.toLowerCase();
		try {
			String x = getText(element).toLowerCase();
			if (element!= null) {
				if (x.toString().contains(text)) {
					Assert.assertTrue(false, element + "Page Not Displayed");
				}
				
			} else {
					System.out.print(false+"Pagediplay is not displaying");
					Assert.assertFalse(false,element +"sdsda");
					Assert.assertTrue(false, element + "Page Not Displayed");
			}
		} catch (Exception e) {
			Assert.assertFalse(false,element +"sdsda");
			System.out.println("Unable to find the element"+e);
		
		}
		Assert.assertTrue(false, element + "Page Not Displayed");
	}
	
	/*
	 * 
	 * 
	 */
	public static String getText(WebElement element) {
		String objectValue = null;
		try {
			objectValue = element.getText();
			objectValue.toLowerCase();
		} catch (Exception e) {
			
		}
		return objectValue;
	}
	
	public static String scolltoelement(WebElement element) {
		try {
			webdriverwaitvisible(100, element);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
		}
		
		return scolltoelement(element);
	}
	
	public static void javacripteecutor(WebElement element) {
		webdriverwaitvisible(100, element);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", element);
}
}